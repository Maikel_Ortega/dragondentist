﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DragonTooth : InteractableItem
{
    public enum TEETH_STATES {NORMAL, CRACKED, BROKEN, REPAIRED }
    public Animator animator;
    public TEETH_STATES currentState = TEETH_STATES.NORMAL;

    private void Awake()
    {
        ChangeState(TEETH_STATES.CRACKED);
    }

    public override bool CanBeInteractedWith()
    {
        return false;
    }

    public override void Interact(PlayerCharacter c)
    {
        base.Interact(c);
        transform.DOPunchScale(Vector3.one * 0.2f, 0.2f);
        if (currentState == TEETH_STATES.CRACKED)
        {
            RepairTo(TEETH_STATES.REPAIRED);
        }
    }

    public void RepairTo(TEETH_STATES target)
    {
        //REPAIR ANIMATION
        ChangeState(target);
    }

    public void ChangeState(TEETH_STATES st)
    {
        currentState = st;
        SetToothGraphic(st);
    }

    public void SetToothGraphic(TEETH_STATES st)
    {
        switch (st)
        {
            case TEETH_STATES.NORMAL:
                animator.SetInteger("STATE", 0);
                break;
            case TEETH_STATES.CRACKED:
                animator.SetInteger("STATE", -1);
                break;
            case TEETH_STATES.BROKEN:
                animator.SetInteger("STATE", -2);
                break;
            case TEETH_STATES.REPAIRED:
                animator.SetInteger("STATE", 0);
                break;
        }
    }
}
