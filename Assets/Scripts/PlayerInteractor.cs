﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractor : MonoBehaviour
{
    List<InteractableItem> pickableItemsInRange;
    const string interactableLayer = "Interactable";
    public InteractableItem selectedItem;
    public Transform itemSelector;
    public GameObject selectorPrefab;
    private bool activated=true;

    private void Awake()
    {
        itemSelector = Instantiate(selectorPrefab).transform;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (activated)
        {
            if (LayerMask.LayerToName(other.gameObject.layer) == interactableLayer)
            {
                var it = other.GetComponentInChildren<InteractableItem>();
                if (it != null)
                { 
                    AddToList(it);
                }
            }
        }
    }

    void AddToList(InteractableItem item)
    {
        if (pickableItemsInRange == null)
        {
            pickableItemsInRange = new List<InteractableItem>();
        }
        pickableItemsInRange.Add(item);        
    }

    void Update()
    {
        if (activated)
        {
            UpdateSelectedItem();
        }        
    }

    public void SetColor(Color mColor)
    {
        itemSelector.GetComponentInChildren<SpriteRenderer>().color = mColor;
    }

    void UpdateSelectedItem()
    {
        if (pickableItemsInRange == null || pickableItemsInRange.Count == 0)
        {
            selectedItem = null;
            itemSelector.gameObject.SetActive(false);
        }
        else
        {
            selectedItem = SelectBestItem();
            if (selectedItem != null)
            { 
                itemSelector.transform.position = selectedItem.transform.position;
                itemSelector.gameObject.SetActive(true);
            }
        }
    }

    InteractableItem SelectBestItem()
    {
        InteractableItem closestItem = null;
        foreach (InteractableItem item in pickableItemsInRange)
        {
            if ((closestItem == null))
            {
                closestItem = item;
            }
            else if (Vector3.Distance(transform.position, item.transform.position) < Vector3.Distance(transform.position, closestItem.transform.position))
            {
                if (item.CanBeInteractedWith())
                {
                    closestItem = item;
                }
            }
        }
        return closestItem;
    }

    private void OnTriggerExit(Collider other)
    {
        if (activated)
        { 
            if (LayerMask.LayerToName(other.gameObject.layer) == interactableLayer)
            {
                RemoveFromList(other.GetComponentInChildren<InteractableItem>());
            }
        }
    }

    private void RemoveFromList(InteractableItem pickableItem)
    {
        pickableItemsInRange.Remove(pickableItem);
    }

    internal void Activate(bool active)
    {        
        activated = active;
        if (!activated)
        {   
            itemSelector.gameObject.SetActive(false);           
        }     
    }
}
