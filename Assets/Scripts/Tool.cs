﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool : PickableItem
{
    
    public override bool CanBeInteractedWith()
    {
        return base.CanBeInteractedWith();
    }

    public override void Drop(PlayerCharacter c)
    {
        base.Drop(c);
    }

    public virtual void Use()
    { 

    }
}
