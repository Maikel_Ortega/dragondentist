﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableItem: MonoBehaviour
{
    

    public virtual bool CanBeInteractedWith()
    {
        return true;
    }

    public virtual void Interact(PlayerCharacter c)
    {
        Debug.Log("BASE INTERACTION");
    }
}
