﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public List<Color> playerColors;
    public int playerCount = 4;
    public void Awake()
    {
        Instance = this;
        playerList = new List<PlayerCharacter>();
    }
    public LevelScriptableData currentLevelData;
    public PlayerConfigScriptableData input;
    public GameObject playerPrefab;
    public GameObject burningFX;

    public List<PlayerCharacter> playerList;
    private void LoadLevelData(LevelScriptableData data)
    { 

    }

    public Color GetColor(int i)
    {
        return playerColors[i];
    }

    public PlayerCharacter CreatePlayer(int i)
    {
        var pGO = Instantiate(playerPrefab);
        pGO.name = "Player_" + i.ToString();
        var p = pGO.GetComponent<PlayerCharacter>();
        p.playerNumber = i;
        playerList.Add(p);
        return p;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            StartGame(playerCount);
        }
    }

    public void LoadLevel()
    {
        LoadLevelData(currentLevelData);
    }

    public void StartGame(int n)
    {
        for (int i = 1; i < n+1; i++)
        {
            var p=  CreatePlayer(i);
            p.SetupPlayer();
            p.transform.position += Vector3.right * i;
        }
    }
}
