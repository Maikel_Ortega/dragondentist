﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool_Hammer : Tool
{
    public Collider boxCastCollider;
    public LayerMask dragonToothFilter;
    public override void Use()
    {
        base.Use();
        owner.UseHammerAnimation();
        RaycastHit info;
        if (Physics.BoxCast(owner.transform.position, new Vector3(1f,1f,5f), owner.transform.forward, out info,Quaternion.identity, 4f, dragonToothFilter))
        {
            var dt = info.collider.GetComponentInChildren<DragonTooth>();
            dt.Interact(owner);
        }

    }
}
