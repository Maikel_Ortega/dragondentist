﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="PlayerInputData", menuName ="Dragon/Create player input data")]
public class PlayerConfigScriptableData : ScriptableObject
{
    [System.Serializable]
    public struct InputAxisByEnum
    {
        public PLAYER_INPUT_AXIS axis;
        public string axisName;
    }

    [System.Serializable]
    public struct InputButtonByEnum
    {
        public PLAYER_INPUT_BUTTON button;
        public string buttonName;
    }

    [System.Serializable]
    public struct PlayerInputConfig
    {
        public List<InputAxisByEnum> axis;
        public List<InputButtonByEnum> buttons;
    }

    public List<PlayerInputConfig> inputConfig;

    public enum PLAYER_INPUT_BUTTON { SHIELD_BUTTON, INTERACT_BUTTON, CANCEL_BUTTON }
    public enum PLAYER_INPUT_AXIS { HORIZONTAL_AXIS , VERTICAL_AXIS }

    private string GetPlayerInputButtonString(int playerNumber, PLAYER_INPUT_BUTTON btn)
    {
        List<InputButtonByEnum> list = inputConfig[playerNumber - 1].buttons;
        string parsedButton = list.Find(x => x.button == btn).buttonName;
        return parsedButton;
    }

    private string GetPlayerInputAxisString(int playerNumber, PLAYER_INPUT_AXIS axis)
    {
        List<InputAxisByEnum> list = inputConfig[playerNumber - 1].axis;
        string parsedAxis = list.Find(x => x.axis== axis).axisName;
        return parsedAxis;
    }


    public bool GetButtonDown(int player, PLAYER_INPUT_BUTTON btn)
    {
        string button = GetPlayerInputButtonString(player, btn);
        return Input.GetButtonDown(button);
    }

    public float GetAxis(int player, PLAYER_INPUT_AXIS axis)
    {
        string a = GetPlayerInputAxisString(player, axis);
        return Input.GetAxis(a);        
    }
}
