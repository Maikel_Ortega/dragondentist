﻿using UnityEngine;
using System.Collections;
using System;

public class FSMTester : MonoBehaviour 
{
	public int stamina = 5;
	StateMachine<FSMTester> fsm;
	TesterSt_Rest restState;
	TesterSt_Attack attackState;

	void Start()
	{
		InitFsm();
	}
	
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.N))
		{
			fsm.DoUpdate();
		}
	}

	void InitFsm()
	{
		restState = new TesterSt_Rest();
		attackState = new TesterSt_Attack();
		fsm = new StateMachine<FSMTester>(this,attackState,null,null);
	}	

	public void Attack()
	{
		Debug.Log("Ataco");
		stamina--;
		Debug.Log("Stamina: "+stamina);
	}

	public void Rest()
	{
		Debug.Log("Descanso");
		stamina++;
		Debug.Log("Stamina: "+stamina);        
    }
}
