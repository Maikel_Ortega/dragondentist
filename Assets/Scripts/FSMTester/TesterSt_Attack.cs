﻿using UnityEngine;
using System.Collections;

public class TesterSt_Attack : State<FSMTester> 
{
	public override void Enter (FSMTester owner)
	{
		Debug.Log("Enter state attack");
	}

	public override void Execute (FSMTester owner)
	{
		owner.Attack();
	}

	public override void Exit (FSMTester owner)
	{
		Debug.Log("Exit state attack");
	}
}
