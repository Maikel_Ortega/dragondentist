﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool_WaterTank : Tool
{
    public ParticleSystem waterParticles;
    public override void Use()
    {
        base.Use();
        waterParticles.Play();
    }
}
