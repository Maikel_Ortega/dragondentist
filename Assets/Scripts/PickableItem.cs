﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableItem : InteractableItem
{
    public Rigidbody mRigidBody;
    public PlayerCharacter owner;
    public enum PICKABLE_ITEM_TYPES
    {
        CARGO,
        TOOL
    }

    public PICKABLE_ITEM_TYPES pickableType = PICKABLE_ITEM_TYPES.CARGO;

    private bool canBePickedUp=true;
    public bool CanBePickedUp { get => canBePickedUp; set => canBePickedUp = value; }
    private BurnableItem burnable;
    void Start()
    {
        burnable = GetComponent<BurnableItem>();
    }

    public virtual void PickUp(PlayerCharacter c)
    {
        Debug.Log("PICKING UP ITEM ");
        c.PickUpItem(this);
        canBePickedUp = false;
        transform.SetParent(c.interactor.transform);
        transform.rotation = Quaternion.LookRotation(c.transform.forward, Vector3.up);
        transform.localPosition = Vector3.zero;
        mRigidBody.isKinematic = true;
        owner = c;
    }

    public virtual void Drop(PlayerCharacter c)
    {
        Debug.Log("DROPPING ITEM ");
        owner = null;
        canBePickedUp = true;
        transform.SetParent(null);
        float force = 5;
        mRigidBody.isKinematic = false;
        mRigidBody.AddForce((Vector3.up + c.interactor.transform.right ).normalized * force,ForceMode.Impulse);
    }

    public override bool CanBeInteractedWith()
    {
        bool isBurning = false;
        if (burnable)
        {
            isBurning = burnable.IsBurning();
        }
        return canBePickedUp && !isBurning;
    }

    public override void Interact(PlayerCharacter c)
    {
        base.Interact(c);
        PickUp(c);
    }
}
